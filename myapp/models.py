from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import AbstractUser



class User(AbstractUser):
    image = models.ImageField(upload_to='profile')
    is_banned = models.CharField(max_length = 1, default = "0") 

class Art(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    description = models.TextField()
    drawing = models.ImageField(upload_to='artwork', null=False)
    publishied_date = models.DateField(auto_now_add = True)
    moderate = models.CharField(max_length=100)


class Comment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    comment = models.TextField()
    artwork = models.ForeignKey(Art, on_delete=models.CASCADE)
    published_date = models.DateField(auto_now_add = True)
    visible = models.CharField(max_length = 1, default = 0)

class UserFollow(models.Model):
    follower = models.ForeignKey(User, on_delete=models.CASCADE, default= "", related_name = "follower")
    following = models.ForeignKey(User, on_delete=models.CASCADE, default = "", related_name = "following")

class UserDescription(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    gender = models.TextField(max_length = 6)
    bio = models.TextField()
    hobby = models.CharField(max_length = 100)
    fav_artist = models.CharField(max_length = 100)
    website = models.CharField(max_length = 100)

class ArtTheftModel(models.Model):
    description = models.CharField(max_length = 100)
    art = models.ForeignKey(Art, on_delete=models.CASCADE)




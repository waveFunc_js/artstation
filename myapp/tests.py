from django.test import TestCase
from django.http import HttpResponse
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core.files.uploadedfile import SimpleUploadedFile
from myapp.models import Art, User
from .forms import SignUpForm, UploadArtForm, CommentForm, UserInfo, artTheftForm
from PIL import Image
from io import BytesIO
from django.core.files.images import ImageFile
from django.core.files.uploadedfile import SimpleUploadedFile
import os
from django.test import Client
from django.urls import reverse


class RedirectTestCase(TestCase):
    def setUp(self):
        user = User.objects.create_user( #generate a user
            username = "test",
            first_name = "test1",
            last_name = "test2",
            email = "test@test.com",   
            password = 'testPassword', 
            is_banned = "0"
        )

    def test_login(self):
        self.c = Client()
        response = self.c.post( reverse('login') , {'username' : "test", 'password' : "testPassword"})
        print(response.status_code, 200)
        self.assertRedirects(response,  reverse('index'))

    def test_user_is_banned(self): #test case for banning user
        test = User.objects.get(username = "test")
        test.is_banned = "1"
        test.save()
        self.c = Client()
        response = self.c.post( reverse('login') , {'username' : "test", 'password' : "testPassword"})
        print(response.status_code, 200)
        self.assertRedirects(response,  reverse('banned'))

class FormTest(TestCase):

    def test_user_sign_in(self):
        self.form = SignUpForm(data = {
            'username' : 'testuser',
            'first_name' : 'test_firsasdasdt', 
            'last_name' : 'test_last', 
            'email' : 'test@teasst.com', 
            'password1' :'@art123something', 
            'password2': '@art123something', 
            }, files = {
            'image': SimpleUploadedFile(name = 'test.png', content=open(os.path.dirname(__file__) + "\\test.png", 'rb').read(), content_type='image/jpeg')
            })

        self.assertTrue(self.form.is_valid())

    def test_artTheftForm(self):
        self.form = artTheftForm(data = {
            'description': 'The art was stolen'
        })
        self.assertTrue(self.form.is_valid())

    def test_artUploadForm(self):
        self.form = UploadArtForm(
            data = {
                'title' : 'This is a title', 
                'description' : 'This is a description',  
                'moderate': 1
            }, files = {
            'drawing': SimpleUploadedFile(name = 'test.png', content=open(os.path.dirname(__file__) + "\\test.png", 'rb').read(), content_type='image/jpeg')
            })
        
        self.assertTrue(self.form.is_valid())

    def test_commentForm(self):
        self.form = CommentForm(
            data = {
                'comment' : 'This is a comment'
            }
        )

        self.assertTrue(self.form.is_valid())

    def test_userBio(self):
        self.form = UserInfo({
            'bio' : 'bios', 
            'gender' : 'Male', 
            'hobby' : 'This is a hobby', 
            'fav_artist': 'This is an artist', 
            'website': 'www.example.com'
        })

        self.assertTrue(self.form.is_valid())



